
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;


public class Main {

    public static void main(String[] args) {

        try {
            final String file  = "js.json";
            //распарсить
            FileReader reader = new FileReader(file);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            long one  = (long) jsonObject.get("one");
            long two  = (long) jsonObject.get("two");
            long three  = (long) jsonObject.get("three");
            //вывод
            System.out.println(jsonObject);
            //вывод значения по ключу
            System.out.println(one+" "+two+" "+three);


            //добавление json в коллекцию и отформатированный вывод
            Scanner s = new Scanner(new File(file));
            ArrayList<Object> list = new ArrayList<Object>();
            Pattern pattern = Pattern.compile("\"(.key+?)\"");
            s.useDelimiter(pattern);
            while(s.hasNext()) {
                list.add(s.next());
            }
            System.out.println(list);
            
        }

        catch (ParseException | FileNotFoundException e){
            System.out.println(e);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
